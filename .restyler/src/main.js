import { createApp } from 'vue'
import App from './App.vue'
import store from './vuex'
import router from './vue-router'


import "./assets/style/app.scss";

const app = createApp(App)
    .use(store)
    .use(router)
    .mount("#app")



