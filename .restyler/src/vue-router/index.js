import {createRouter, createWebHistory} from 'vue-router'
import home from "../pages/homePage.vue";
import favorites from "../pages/favorites.vue";
import filmDetail from "../pages/filmDetail.vue";
import search from "../pages/filmsSearch.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: home,
        meta: {
            title: "GTS-TEST",
        }
    },
    {
        path: '/favorites',
        name: 'favorites',
        component: favorites,
        meta: {
            title: "GTS-TEST",
        }
    },
    {
        path: '/search',
        name: 'search',
        component: search,
        meta: {
            title: "GTS-TEST",
        }
    },
    {
        path: '/film/:id',
        name: 'filmDetail',
        component: filmDetail,
        props: true,
        meta: {
            title: "GTS-TEST",
        }
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router