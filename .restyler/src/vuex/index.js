import {createStore} from 'vuex'
import searchFilms from "./modules/searchFilms"
import films from "./modules/films"
import favorites from "./modules/favorites"

export default createStore({
    modules: {
        searchFilms,
        films,
        favorites
    },
})

