import axios from "axios";

export default ({
    actions: {
        GET_FAVORITES({commit}) {
            axios('https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_250_BEST_FILMS&page=1', {
                method: 'GET',
                headers: {
                    'accept': 'application/json',
                    'X-API-KEY': 'c424ca56-0ead-4b61-abf0-96cd927e07a8',
                },
            }).then((response) => {
                commit('UPDATE_FAVORITES', response.data.films)
            }).catch((error) => {
                return error;
            })
        },
    },
    mutations: {
        UPDATE_FAVORITES_ID(state, localStorageId) {
            let index = state.localStorageIds.indexOf(localStorageId)
            if (index >= 0) {
                state.localStorageIds.splice(index, 1)
            } else state.localStorageIds.push(localStorageId)

        },
        SET_FAVORITES_IDS(state, ids) {
            state.localStorageIds = ids
        },
        UPDATE_FAVORITES(state, favorites) {
            const filteredFilms = [];
            favorites.forEach((item) => {
                CheckIds(item)
            })

            function CheckIds(film) {
                state.localStorageIds.forEach((item) => {
                    if (item === film.filmId.toString()) {
                        filteredFilms.push(film)
                    }
                })
            }

            state.favorites = filteredFilms
            console.log(state.favorites)
        },
    },
    state: {
        localStorageIds: [],
        favorites: [],
    },
    getters: {
        getLocalStorageId(state) {
            return state.localStorageIds
        },
        getFavorites(state){
            return state.favorites
        }
    }
})