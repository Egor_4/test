import axios from "axios";

export default ({
    actions: {
        GET_FILM({commit}, filmId) {
            axios('https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_250_BEST_FILMS&page=1', {
                method: 'GET',
                headers: {
                    'accept': 'application/json',
                    'X-API-KEY': 'c424ca56-0ead-4b61-abf0-96cd927e07a8',
                },
            }).then((response) => {
                response.data.films.forEach((item) => {
                    if (item.filmId.toString() === filmId) {
                        commit('SET_FILM', item);
                    }
                })
            }).catch((error) => {
                return error;
            })
        },
    },
    mutations: {
        SET_FILM: (state, film) => {
            state.film = film
        },
    },
    state: {
        film: {},
    },
    getters: {
        getFilm(state){
            return state.film
        }
    }
})