import axios from "axios";

export default ({
    actions: {
        GET_SEARCHED_FILMS({commit}) {
            commit('SET_LOADING_STATUS', true);
            const delay = async (ms) => await new Promise(resolve => setTimeout(resolve, ms));
            delay(1000).then(() => axios('https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_250_BEST_FILMS&page=1', {
                method: 'GET',
                headers: {
                    'accept': 'application/json',
                    'X-API-KEY': 'c424ca56-0ead-4b61-abf0-96cd927e07a8',
                },
            }).then((response) => {
                commit('SET_SEARCHED_FILMS', response.data.films);
            }).catch((error) => {
                return error;
            })
                .finally(() => {
                    commit('SET_LOADING_STATUS', false);
                }))
        },
    },
    mutations: {
        SET_SEARCHED_FILMS: (state, films) => {
            const filteredFilms = [];
            films.forEach((item) => {
                if (item.nameRu.toLowerCase().includes(state.searchValue.toLowerCase())) {
                    filteredFilms.push(item)
                }
            })
            state.FoundFilms = filteredFilms;
        },
        SET_SEARCH_VALUE: (state, searchValue) => {
            state.searchValue = searchValue
        },
        SET_LOADING_STATUS(state, loadingStatus) {
            state.loadingStatus = loadingStatus
        }
    },
    state: {
        FoundFilms: [],
        searchValue: '',
        loadingStatus: false,
    },
    getters: {
        getFoundFilms(state) {
            return state.FoundFilms
        },
        getFilmById(state) {
            return (filmId) => {
                return state.FoundFilms.find(element => element.filmId == filmId)
            }
        },
        getSearchValue(state){
            return state.searchValue
        },
        getLoadingStatus(state) {
            return state.loadingStatus
        }
    }
})